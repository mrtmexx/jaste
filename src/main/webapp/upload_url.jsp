<!DOCTYPE html>
<html>
    <head>
        <title>File Upload Form</title>
    </head>
<body>

<h1>Upload file</h1>
<form method="POST" action="url">
<div class="control">
    <label for="url">Enter an https:// URL:</label>
    <input type="url" name="url" id="url"
           placeholder="https://example.com"
           pattern="https://.*" size="20" required />
</div>
<input type="submit" value="Submit">
</form>

</body>
</html>