package ru.tmpc.upload;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet(name = "imageUploadServlet", urlPatterns = { "upload" }, loadOnStartup = 1)
@MultipartConfig(fileSizeThreshold = 6291456, // 6 MB
		maxFileSize = 10485760L, // 10 MB
		maxRequestSize = 20971520L // 20 MB
)
public class imageUploadServlet extends HttpServlet {
	
	private static final String UPLOAD_DIR = "/tmp/uploads";
	private static final String[] contentsType = new String[]{"image/jpeg", "image/svg+xml", "image/gif", "image/png"};

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("upload_file.jsp").forward(request, response);

    }


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		// constructs path of the directory to save uploaded file
		String uploadFilePath = UPLOAD_DIR;

		// creates upload folder if it does not exists
		File uploadFolder = new File(uploadFilePath);
		if (!uploadFolder.exists()) {
			uploadFolder.mkdirs();
		}

		PrintWriter writer = response.getWriter();

		// write all files in upload folder
		for (Part part : request.getParts()) {
			if (part != null && part.getSize() > 0) {
				String fileName = part.getSubmittedFileName();
				String contentType = part.getContentType();
				
				// allows only JPEG files to be uploaded
				if(!checkContentType(contentType)){
					response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
				}
				
				part.write(uploadFilePath + File.separator + fileName);
				String fileUrl = request.getRequestURL() + File.separator + fileName;
				writer.append("File successfully uploaded to <a href=\"" + fileUrl + "\">" + fileUrl + "</a>");
			}
		}

	}

	public static boolean checkContentType(String contentType) {
		List<String> listContentType = Arrays.asList(contentsType);
		if (!listContentType.contains(contentType)) {
			System.out.println("ERROR: not allow contentType:" + contentType);
			return false;
		}
		return true;
	}

}