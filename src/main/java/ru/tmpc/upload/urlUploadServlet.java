package ru.tmpc.upload;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.security.MessageDigest;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@WebServlet(name = "urlUploadServlet", urlPatterns = { "url" }, loadOnStartup = 1)
public class urlUploadServlet extends HttpServlet {

	private static final long serialVersionUID = 5619951677845873534L;
	
	private static final String UPLOAD_DIR = "/tmp/uploads";

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		request.getRequestDispatcher("upload_url.jsp").forward(request, response);

    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		// constructs path of the directory to save uploaded file
		String uploadFilePath = UPLOAD_DIR;

		// creates upload folder if it does not exists
		File uploadFolder = new File(uploadFilePath);
		if (!uploadFolder.exists()) {
			uploadFolder.mkdirs();
		}

		PrintWriter writer = response.getWriter();
		String url = request.getParameter("url");
		System.out.println("Url: " + url);
		if(!isNullOrEmpty(url)) {
			String fileUploadUrl = saveImage(url, uploadFilePath);
			String fileName = url.substring( url.lastIndexOf('/')+1, url.length() );
			System.out.println(getStringFromSHA256(fileName));
			writer.append("File successfully uploaded to <a href=\"" + fileUploadUrl + "\">" + fileUploadUrl + "</a>");
		}
	}

	private static boolean isNullOrEmpty(String str) {
        if(str != null && !str.isEmpty())
            return false;
        return true;
	}
	
	private static String saveImage(String imageUrl, String dstDir) throws IOException {
		URL url = new URL(imageUrl);
		String fileName = url.getFile();
		String destName = dstDir + fileName.substring(fileName.lastIndexOf("/"));
	 
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(destName);
	 
		byte[] b = new byte[2048];
		int length;
	 
		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}
	 
		is.close();
		os.close();

		return fileName;
	}

	public String generateUniqFileName(String fileName) {
		String fileNameExt = fileName.substring(fileName.lastIndexOf('.'), fileName.length());
 		String fileNameWithoutExtn = fileName.substring(0, fileName.lastIndexOf('.'));
		
		return "lolo";
	}

	public static String getStringFromSHA256(String stringToEncrypt) {
		String toReturn = null;

		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(stringToEncrypt.getBytes(StandardCharsets.UTF_8));
			toReturn = Base64.getEncoder().encodeToString(hash);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return toReturn;
	}
}